/*
 * CCVideoPlayer
 *
 * Cocos2D-iPhone-Extensions v0.2.1
 * https://github.com/cocos2d/cocos2d-iphone-extensions
 *
 * Copyright (c) 2010-2012 Stepan Generalov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
#import <UIKit/UIKit.h>

// VideoOverlayView is view that added on video view and cancels video on touch.
@interface VideoOverlayView : UIView {
    
    BOOL _touch;

}


- (void)apron7049;


- (void)distinguish7051;


- (void)section7053;


- (void)reimburse7055;


- (void)pivotal7057;


- (void)civilian7059;


- (void)portraiture7061;


- (void)adjure7063;


- (void)determinant7065;


- (void)improvise7067;


- (void)incommunicative7069;


- (void)persuasive7071;


- (void)divergence7073;


- (void)rationalize7075;


- (void)olfaction7077;


- (void)suddenly7079;


- (void)illustration7081;


- (void)carat7083;


- (void)conduct7085;


- (void)efficacious7087;


- (void)impugn7089;


- (void)strike7091;


- (void)genial7093;


- (void)blurb7095;


- (void)marry7097;


- (void)aloof7099;


- (void)adulterate7101;


- (void)perspective7103;


- (void)dissonant7105;


- (void)prickle7107;


- (void)Irish7109;


- (void)minicomputer7111;


- (void)behold7113;


- (void)partially7115;


- (void)emphasis7117;


- (void)identity7119;


- (void)paralyse7121;


- (void)booming7123;


- (void)gemstone7125;


- (void)irksome7127;


- (void)manifestation7129;


- (void)cultural7131;


- (void)sniff7133;


- (void)superstructure7135;


- (void)retrieve7137;


- (void)substantial7139;


- (void)dictation7141;


- (void)intersperse7143;


- (void)proboscis7145;


- (void)assess7147;


- (void)regal7149;


- (void)vulpine7151;


- (void)friendship7153;


- (void)lance7155;


- (void)somehow7157;


- (void)queer7159;


- (void)prior7161;


- (void)penitentiary7163;


- (void)adjourn7165;


- (void)precept7167;


- (void)temperance7169;


- (void)outlet7171;


- (void)education7173;


- (void)lugubriousness7175;


- (void)forgive7177;


- (void)sketch7179;


- (void)radish7181;


- (void)indemnity7183;


- (void)ineligible7185;


- (void)fracas7187;


- (void)carapace7189;


- (void)covert7191;


- (void)ponderable7193;


- (void)voltage7195;


- (void)personal7197;


- (void)trade7199;


- (void)reagent7201;


- (void)magnanimous7203;


- (void)indignity7205;


- (void)retentive7207;


- (void)brighten7209;


- (void)dunce7211;


- (void)terminus7213;


- (void)shackle7215;


- (void)retrospect7217;


- (void)festal7219;


- (void)undergraduate7221;


- (void)element7223;


- (void)reluctant7225;


- (void)retention7227;


- (void)daybreak7229;


- (void)improbity7231;


- (void)prominence7233;


- (void)prevalent7235;


- (void)technocrat7237;


- (void)actual7239;


- (void)grounded7241;


- (void)exemption7243;


- (void)muscle7245;


- (void)ultraviolet7247;


- (void)tutelage7249;


- (void)glass7251;


- (void)girder7253;


- (void)enrage7255;


- (void)staccato7257;


- (void)volley7259;


- (void)incompatibility7261;


- (void)yeoman7263;


- (void)epitome7265;


- (void)ruthless7267;


- (void)pendulum7269;


- (void)cautious7271;


- (void)befuddlement7273;


- (void)nomadic7275;


- (void)feign7277;


- (void)resume7279;


- (void)faculty7281;


- (void)ashtray7283;


- (void)reactionary7285;


- (void)tedium7287;


- (void)nymph7289;


- (void)deaden7291;


- (void)payable7293;


- (void)squeamish7295;


- (void)abstention7297;


- (void)entire7299;


- (void)dedicate7301;


- (void)inflamed7303;


- (void)testify7305;


- (void)fatal7307;


- (void)bogus7309;


- (void)envelop7311;


- (void)vituperative7313;


- (void)projection7315;


- (void)craftsmanship7317;


- (void)disinclined7319;


- (void)illegal7321;


- (void)genuine7323;


- (void)deliverance7325;


- (void)cultural7327;


- (void)abstention7329;


- (void)devotion7331;


- (void)teacher7333;


- (void)pronounce7335;


- (void)delegate7337;


- (void)enzyme7339;


- (void)ulcerate7341;


- (void)congress7343;


- (void)missionary7345;


- (void)prospect7347;


- (void)transportation7349;


- (void)paralyze7351;


- (void)background7353;


- (void)conclusion7355;


- (void)pliable7357;


- (void)parlous7359;


- (void)according7361;


- (void)discrete7363;


- (void)survival7365;


- (void)teenager7367;


- (void)tackiness7369;


- (void)tackiness7371;


- (void)relay7373;


- (void)traumatic7375;


- (void)offend7377;


- (void)consult7379;


- (void)indefatigable7381;


- (void)intrude7383;


- (void)integrate7385;


- (void)secret7387;


- (void)reading7389;


- (void)victor7391;


- (void)highlight7393;


- (void)ample7395;


- (void)wraith7397;


- (void)horticulture7399;


- (void)clinch7401;


- (void)troop7403;


- (void)stockade7405;


- (void)potentiate7407;


- (void)anodyne7409;


- (void)aesthete7411;


- (void)trinket7413;


- (void)attributive7415;


- (void)rancid7417;


- (void)poverty7419;


- (void)senator7421;


- (void)filings7423;


- (void)dinghy7425;


- (void)adjustment7427;


- (void)macroeconomics7429;


- (void)utility7431;


- (void)heliotrope7433;


- (void)auspices7435;


- (void)negligence7437;


- (void)pneumatic7439;


- (void)recess7441;


- (void)jaded7443;


- (void)exhaustion7445;


- (void)infatuation7447;


- (void)soldier7449;


- (void)courtroom7451;


- (void)plodding7453;


- (void)outwit7455;


- (void)parse7457;


- (void)primate7459;


- (void)visitor7461;


- (void)accompany7463;


- (void)bellicose7465;


- (void)tactic7467;


- (void)fledge7469;


- (void)alliteration7471;


- (void)truthful7473;


- (void)soprano7475;


- (void)anodyne7477;


- (void)propinquity7479;


- (void)fluid7481;


- (void)canard7483;


- (void)bifurcate7485;


- (void)package7487;


- (void)distension7489;


- (void)disobey7491;


- (void)discernible7493;


- (void)rebellious7495;


- (void)uninterested7497;


- (void)various7499;


- (void)intelligent7501;


- (void)vestigial7503;


- (void)jejune7505;


- (void)engross7507;


- (void)Ireland7509;


- (void)narcissism7511;


- (void)polarization7513;


- (void)impair7515;


- (void)knowledge7517;


- (void)dirge7519;


- (void)pillow7521;


- (void)weight7523;


- (void)palatable7525;


- (void)skyscraper7527;


- (void)precursor7529;


- (void)pathos7531;


- (void)liberation7533;


- (void)episodic7535;


- (void)dissection7537;


- (void)ventral7539;


- (void)mattress7541;


- (void)domain7543;


- (void)favorite7545;


- (void)consumption7547;


- (void)insistence7549;


- (void)obliterate7551;


- (void)scrutable7553;


- (void)those7555;


- (void)misshapen7557;


- (void)seemingly7559;


- (void)apologist7561;


- (void)comestible7563;


- (void)sound7565;


- (void)mingle7567;


- (void)spoke7569;


- (void)package7571;


- (void)guile7573;


- (void)marketplace7575;


- (void)screed7577;


- (void)chief7579;


- (void)tablet7581;


- (void)trickle7583;


- (void)cream7585;


- (void)torque7587;


- (void)elemental7589;


- (void)clemency7591;


- (void)pusillanimous7593;


- (void)cocktail7595;


- (void)leonine7597;


- (void)impress7599;


- (void)tickle7601;


- (void)tractability7603;


- (void)airplane7605;


- (void)circumference7607;


- (void)disseminate7609;


- (void)vulnerability7611;


- (void)biosphere7613;


- (void)repartee7615;


- (void)frightful7617;


- (void)effectiveness7619;


- (void)obstinate7621;


- (void)rueful7623;


- (void)smelt7625;


- (void)finance7627;


- (void)sever7629;


- (void)satiny7631;


- (void)resolutely7633;


- (void)hydrate7635;


- (void)drainage7637;


- (void)intercourse7639;


- (void)morality7641;


- (void)diminuendo7643;


- (void)shake7645;


- (void)adolescent7647;


- (void)spunk7649;


- (void)ahead7651;


- (void)alabaster7653;


- (void)pecan7655;


- (void)decide7657;


- (void)fussy7659;


- (void)language7661;


- (void)recline7663;


- (void)dawdle7665;


- (void)tonight7667;


- (void)collective7669;


- (void)deportation7671;


- (void)pronounced7673;


- (void)student7675;


- (void)substantiate7677;


- (void)halve7679;


- (void)finalize7681;


- (void)thresh7683;


- (void)misunderstand7685;


- (void)withdrawal7687;


- (void)freeze7689;


- (void)malpractice7691;


- (void)exact7693;


- (void)disbarment7695;


- (void)fillet7697;


- (void)ostentatious7699;


- (void)dilapidate7701;


- (void)underhanded7703;


- (void)noise7705;


- (void)chivalrous7707;


- (void)reparation7709;


- (void)dilatory7711;


- (void)immoral7713;


- (void)infinitive7715;


- (void)frankly7717;


- (void)nectar7719;


- (void)etching7721;


- (void)abrasive7723;


- (void)hoary7725;


- (void)arrhythmic7727;


- (void)perjure7729;


- (void)principle7731;


- (void)fastidious7733;


- (void)unimpressive7735;


- (void)enhance7737;


- (void)spruce7739;


- (void)transient7741;


- (void)scandalous7743;


- (void)arduous7745;


- (void)waspish7747;


- (void)sheen7749;


- (void)purport7751;


- (void)hoarse7753;


- (void)promenade7755;


- (void)excavate7757;


- (void)unfortunately7759;


- (void)charitable7761;


- (void)rapport7763;


- (void)peptic7765;


- (void)Byzantine7767;


- (void)gargantuan7769;


- (void)compassion7771;


- (void)obscure7773;


- (void)glitch7775;


- (void)cleanse7777;


- (void)noticeable7779;


- (void)surveillance7781;


- (void)stasis7783;


- (void)eccentricity7785;


- (void)calcify7787;


- (void)subsidiary7789;


- (void)representative7791;


- (void)saturated7793;


- (void)erupt7795;


- (void)modest7797;


- (void)frame7799;


- (void)lament7801;


- (void)satisfaction7803;


- (void)distill7805;


- (void)clarification7807;


- (void)deracinate7809;


- (void)remote7811;


- (void)trawl7813;


- (void)supplement7815;


- (void)handicapped7817;


- (void)broken7819;


- (void)corrosive7821;


- (void)subjunctive7823;


- (void)poetry7825;


- (void)lascivious7827;


- (void)decipher7829;


- (void)rudiments7831;


- (void)contort7833;


- (void)equity7835;


- (void)cardiologist7837;


- (void)abstracted7839;


- (void)bedraggled7841;


- (void)censorship7843;


- (void)mercenary7845;


- (void)stickler7847;


- (void)menial7849;


- (void)enroll7851;


- (void)behold7853;


- (void)recognize7855;


- (void)judge7857;


- (void)blasphemy7859;


- (void)suppression7861;


- (void)poniard7863;


- (void)congress7865;


- (void)porch7867;


- (void)nurture7869;


- (void)salvage7871;


- (void)nobody7873;


- (void)cavern7875;


- (void)Malaysia7877;


- (void)potshot7879;


- (void)celebration7881;


- (void)pointless7883;


- (void)reckless7885;


- (void)continual7887;


- (void)tainted7889;


- (void)congregate7891;


- (void)housework7893;


- (void)grand7895;


- (void)adherent7897;


- (void)disinclined7899;


- (void)glitch7901;


- (void)endeavor7903;


- (void)highlight7905;


- (void)solvent7907;


- (void)osseous7909;


- (void)marrow7911;


- (void)homework7913;


- (void)marionette7915;


- (void)lineal7917;


- (void)affront7919;


- (void)monologue7921;


- (void)crook7923;


- (void)wince7925;


- (void)moratorium7927;


- (void)reverie7929;


- (void)dedicate7931;


- (void)smash7933;


- (void)Mexican7935;


- (void)prefix7937;


- (void)demand7939;


- (void)affirmative7941;


- (void)subtract7943;


- (void)corporeal7945;


- (void)sacrifice7947;


- (void)encyclopedia7949;


- (void)fanfare7951;


- (void)gardener7953;


- (void)discretionary7955;


- (void)entity7957;


- (void)harbinger7959;


- (void)intuition7961;


- (void)muzzy7963;


- (void)mighty7965;


- (void)collide7967;


- (void)intertwine7969;


- (void)fiddle7971;


- (void)journey7973;

@end

#endif
