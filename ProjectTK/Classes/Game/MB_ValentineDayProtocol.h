//
//  MB_ValentineDayProtocol.h
//  ProjectMB
//
//  Created by wenyong on 15-1-22.
//
//

#ifndef __ProjectMB__MB_ValentineDayProtocol__
#define __ProjectMB__MB_ValentineDayProtocol__

#include "MB_FestivalProtocol.h"

#define SEND_VALENTINEDAY_INFO SEND_FESTIVAL_INFO
#define RECV_VALENTINEDAY_INFO RECV_FESTIVAL_INFO
#define SEND_VALENTINEDAY_CLICK SEND_FESTIVAL_CLICK
#define RECV_VALENTINEDAY_CLICK RECV_FESTIVAL_CLICK
#define SEND_VALENTINEDAY_RANK SEND_FESTIVAL_RANK
#define RECV_VALENTINEDAY_RANK RECV_FESTIVAL_RANK


typedef MB_FestivalRankData MB_ValentineDayRankData;
typedef MB_FestivalRankReward MB_ValentineDayRankReward;

#endif /* defined(__ProjectMB__MB_ValentineDayProtocol__) */
