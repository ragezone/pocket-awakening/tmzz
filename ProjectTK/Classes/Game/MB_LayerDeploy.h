//
//  MB_LayerDeploy.h
//  ProjectMB
//
//  Created by yuanwugang on 14-11-22.
//
//

#ifndef __ProjectMB__MB_LayerDeploy__
#define __ProjectMB__MB_LayerDeploy__

#include "MB_ResWindow.h"

class MB_LayerDeploy : public CCLayer
{
public:
    virtual void onClickNode() = 0;
};

#endif /* defined(__ProjectMB__MB_LayerDeploy__) */
