

#ifndef ProjectMB_MB_DefineProto_h
#define ProjectMB_MB_DefineProto_h

#define CS_FIGHT_REQUEST 20001
#define P_ACTION 20004
#define P_FIGHTER 20003
#define SC_FIGHT_REQUEST 20002
#define CS_LOG 12301
#define CS_GIFT_REQUEST 12201
#define P_GER_VIEW 10418
#define P_ITEM_VIEW 10616
#define P_REWARD_INFO 11930
#define SC_GIFT_REQUEST 12202
#define CS_KING_ENTER 12101
#define SC_KING_ENTER 12102
#define SC_KING_ENTER_WAIT_FIRST_SESSION 12103
#define CS_KING_QUIT 12106
#define CS_KING_SIGN 12110
#define SC_KING_SIGN 12111
#define CS_KING_BUFF 12120
#define SC_KING_BUFF 12121
#define P_REC 12131
#define SC_KING_NEW_FIGHT 12130
#define CS_KING_REPLAY 12140
#define SC_KING_REPLAY 12141
#define CS_KING_HISTORY 12150
#define SC_KING_HISTORY 12151
#define CS_KING_RANK 12160
#define P_KING_RANK 12162
#define SC_KING_RANK 12161
#define CS_BOX_ITEM 12001
#define P_REWARD_VIEW 12005
#define SC_BOX_ITEM 12002
#define CS_BOX_SHOP 12003
#define SC_BOX_SHOP 12004
#define CS_BOX_GET_SPRIT_EQUIP_COUNT 12011
#define SC_BOX_GET_SPRIT_EQUIP_COUNT 12012
#define CS_BOX_SHOP_INFO 12007
#define SC_BOX_SHOP_INFO 12008
#define P_REWARD_VIEW2 12006
#define CS_ACTIVITY_GET_LIST 11901
#define P_ACTIVITY_ICON 11903
#define SC_ACTIVITY_GET_LIST 11902
#define CS_ACTIVITY_INFO 11910
#define P_ACTIVITY_DRAW 11912
#define SC_ACTIVITY_INFO 11911
#define CS_ACTIVITY_DRAW 11920
#define SC_ACTIVITY_DRAW 11921
#define SC_ACTIVITY_UPDATE 11940
#define SC_ACTIVITY_RECORD_UPDATE 11941
#define CS_INVITE_INFO 11801
#define SC_INVITE_INFO 11802
#define CS_INVITE_BIND_WEIBO 11810
#define SC_INVITE_BIND_WEIBO 11811
#define CS_INVITE_WEIBO_SHARE_LEVELUP 11812
#define SC_INVITE_WEIBO_SHARE_LEVELUP 11813
#define CS_INVITE_INPUT_INVITE_CODE 11820
#define SC_INVITE_INPUT_INVITE_CODE 11821
#define CS_INVITE_LIST 11830
#define P_INVITE 11832
#define SC_INVITE_LIST 11831
#define CS_FRIEND_GET_LIST 11701
#define P_FRIEND 11703
#define SC_FRIEND_GET_LIST 11702
#define CS_FRIEND_GET_ADD_LIST 11706
#define SC_FRIEND_GET_ADD_LIST 11707
#define CS_FRIEND_ADD 11708
#define SC_FRIEND_ADD 11709
#define CS_FRIEND_EXPLORE 11720
#define SC_FRIEND_EXPLORE 11721
#define CS_FRIEND_DELETE 11730
#define SC_FRIEND_DELETE 11731
#define SC_FRIEND_NOTIFY_DELETE 11732
#define SC_FRIEND_NEW 11740

#define CS_FRIEND_FIGHT     11704
#define SC_FRIEND_FIGHT     11705
#define CS_FRIEND_SEND_ENERGY 11741
#define SC_FRIEND_SEND_ENERGY 11742
#define SC_FRIEND_SEND_ENERGY_ME 11743
#define CS_FRIEND_GIVE_ENERGY 11744
#define SC_FRIEND_GIVE_ENERGY 11745
#define SC_FRIEND_GIVE_ENERGY_ME 11746
#define CS_FRIEND_GIVE_ALL_ENERGY 11747

#define CS_FRIEND_ADD_LIST 11711    //好友請求消息列表
#define SC_FRIEND_ADD_LIST 11712
#define SC_FRIEND_NEW_ADD  11713    //新增好友請求消息請求
#define CS_FRIEND_AGREE    11714    //同意加好友
#define SC_FRIEND_AGREE    11715
#define CS_FRIEND_REFUSE   11716    //拒絕加好友
#define SC_FRIEND_REFUSE   11717

#define CS_GATHER_GET_LIST 11601
#define SC_GATHER_GET_LIST 11602
#define SC_GATHER_NEW 11603
#define CS_HIST_GET_LIST 11501
#define P_HIST 11503
#define SC_HIST_GET_LIST 11502
#define CS_HIST_MORE 11510
#define SC_HIST_MORE 11511
#define CS_HIST_REPLAY 11520
#define SC_HIST_REPLAY 11521
#define SC_HIST_UNREADNUM 11531
#define CS_MAIL_INFO 11401
#define P_ID_NUM 11403
#define P_MAIL_REWARD 11404
#define P_MAIL 11405
#define SC_MAIL_INFO 11402
#define CS_MAIL_DRAW_REWARD 11406
#define SC_MAIL_DRAW_REWARD 11407
#define CS_MAIL_DELETE 11408
#define SC_MAIL_DELETE 11409
#define CS_MAIL_NEW 11410
#define SC_MAIL_NEW 11411
#define CS_MAIL_UNREAD_NUM 11420
#define SC_MAIL_UNREAD_NUM 11421
#define CS_MAIL_MORE 11430
#define SC_MAIL_MORE 11431
#define CS_MAIL_AGREE_FRIEND 11440
#define SC_MAIL_AGREE_FRIEND 11441
#define SC_FRIEND_REMOVE_REQUEST 11748
#define CS_HRON_INFO 11301
#define SC_HRON_INFO_WAIT 11302
#define SC_HRON_INFO_STOP 11303
#define CS_HRON_INFO_ON 11304
#define SC_HRON_INFO_ON 11305
#define CS_HRON_LAST_RANK_LIST 11311
#define P_HRON_ROLE 11315
#define SC_HRON_LAST_RANK_LIST 11312
#define CS_HRON_CUR_RANK_LIST 11313
#define SC_HRON_CUR_RANK_LIST 11314
#define CS_HRON_BUY 11321
#define SC_HRON_BUY 11322
#define CS_HRON_FIGHT 11331
#define P_GER_ADD_EXP 10211
#define P_REWARD 10210
#define SC_BATTLE_BROADCAST_GET_ITEM 10212
#define SC_HRON_FIGHT 11332
#define SC_HRON_STOP 11341
#define CS_HRON_RANK 11351
#define SC_HRON_RANK 11352
#define CS_HRON_OPEN_TIME 11360
#define SC_HRON_OPEN_TIME 11361
#define CS_HRON_SUCC_REWARD 11370
#define SC_HRON_SUCC_REWARD 11371
#define CS_HULA_OPEN 11201
#define SC_HULA_OPEN 11202
#define SC_HULA_INIT_STATE 11203
#define CS_HULA_CLOSE 11204
#define CS_HULA_BUFF 11205
#define SC_HULA_BUFF 11206
#define CS_HULA_LAST_INFO 11211
#define SC_HULA_LAST_INFO_IGNORE 11212
#define P_HULA_INFO 11215
#define SC_HULA_LAST_INFO_WIN 11213
#define SC_HULA_LAST_INFO_FAIL 11214
#define CS_HULA_CUR_INFO 11216
#define SC_HULA_CUR_INFO_IGNORE 11217
#define SC_HULA_CUR_INFO 11218
#define SC_HULA_HP_SYNC 11220
#define P_HULA_HARM 11222
#define SC_HULA_HARM_BROADCAST 11221
#define SC_HULA_BUFF_SYNC 11224
#define SC_HULA_STOP 11225
#define CS_HULA_RANK_SYNC 11226
#define SC_HULA_RANK_SYNC 11227
#define CS_HULA_FIGHT 11231
#define SC_HULA_FIGHT 11232
#define CS_HULA_REBORN 11241
#define SC_HULA_REBORN 11242
#define CS_HULA_OFFLINE_PLAY 11243
#define SC_HULA_OFFLINE_PLAY 11244
#define CS_HULA_OPEN_TIME 11250
#define SC_HULA_OPEN_TIME 11251

#define CS_DAILY_GET_LIST 11001
#define P_DAILY 11003
#define SC_DAILY_GET_LIST 11002
#define CS_DAILY_DRAW 11005
#define SC_DAILY_DRAW 11006
#define CS_PLUNDER_INFO 10901
#define P_PLUNDER 10903
#define SC_PLUNDER_INFO 10902
#define CS_PLUNDER_STOP_FREE 10904
#define SC_PLUNDER_STOP_FREE 10905
#define SC_PLUNDER_UPDATE_FREE_TIME 10906
#define CS_PLUNDER_BEGIN 10907
#define SC_PLUNDER_BEGIN 10908
#define SC_PLUNDER_BREAK 10909
#define CS_PLUNDER_COLLECT 10910
#define SC_PLUNDER_COLLECT 10911
#define CS_PLUNDER_GET_TARGET 10912
#define P_PLUNDER_TAR 10914
#define SC_PLUNDER_GET_TARGET 10913
#define CS_PLUNDER_FIGHT 10915
#define SC_PLUNDER_FIGHT 10916
#define SC_PLUNDER_FINISH 10917
#define CS_PLUNDER_CANCEL_BREAK 10918
#define SC_PLUNDER_CANCEL_BREAK 10919
#define CS_PLUNDER_GET_TARGET_BY_COMPOSE 10920
#define SC_PLUNDER_GET_TARGET_BY_COMPOSE 10921
#define P_PLUNDER_TARGET    10922
#define CS_PVP_GET_LIST 10801
#define P_PVP 10803
#define SC_PVP_GET_LIST 10802
#define CS_PVP_FIGHT 10804
#define SC_PVP_FIGHT 10805
#define CS_PVP_GET_FIRST_EIGHT_REPLAYS 10806
#define SC_PVP_GET_FIRST_EIGHT_REPLAYS 10807
#define P_PVP_REPLAY_INFO 10808
#define CS_PVP_EIGHT_REPLAY 10809
#define SC_PVP_EIGHT_REPLAY 10810
#define CS_SHOP_BUY_NUM 10701
#define P_SHOP_NUM 10703
#define SC_SHOP_BUY_NUM 10702
#define CS_SHOP_BUY 10704
#define SC_SHOP_BUY 10705
#define CS_SHOP_ENCOUNTER 10710
#define P_SHOP_RANDOM 10712
#define SC_SHOP_ENCOUNTER 10711
#define SC_SHOP_NEW 10713
#define CS_SHOP_REFRESH 10720
#define SC_SHOP_REFRESH 10721
#define SC_SHOP_AUTO_REFRESH 10730
#define CS_ITEM_BAG 10601
#define P_ITEM 10603
#define SC_ITEM_BAG 10602
#define CS_ITEM_EQUIP 10604
#define P_EQUIP 10606
#define SC_ITEM_EQUIP 10605
#define CS_ITEM_SELL 10607
#define SC_ITEM_SELL 10608
#define CS_ITEM_DOWN_EQUIP 10609
#define SC_ITEM_DOWN_EQUIP 10610
#define CS_ITEM_UP_EQUIP 10611
#define SC_ITEM_UP_EQUIP 10612
#define SC_ITEM_NEW 10613
#define P_ITEM_NUM_UPDATE 10615
#define SC_ITEM_UPDATE 10614
#define CS_ITEM_USE 10617
#define SC_ITEM_USE 10618
#define SC_ITEM_DELETE_NOTIFY 10619
#define CS_ITEM_REINFORCE 10620
#define SC_ITEM_REINFORCE 10621
#define CS_ITEM_MAX_REINFORCE 10622
#define SC_ITEM_MAX_REINFORCE 10623
#define SC_ITEM_UPDATE_RANK 10624
#define CS_ITEM_UP_RANK 10625
#define SC_ITEM_UP_RANK 10626
#define SC_ITEM_MORE 10627
#define CS_ITEM_COMPOUND 10631
#define SC_ITEM_COMPOUND 10632
#define CS_ITEM_EAT 10633
#define SC_ITEM_EAT 10634
#define CS_ROLE_LOGIN_REWARD 10139
#define SC_ROLE_LOGIN_REWARD 10140
#define CS_ITEM_USE_INFO 10637   //体力道具使用次數
#define SC_ITEM_USE_INFO 10638
#define CS_EXPLORE_ONE 10501
#define P_ECHAPTER 10503
#define SC_EXPLORE_ONE 10502
#define CS_EXPLORE_DUNGEON_LIST 10504
#define P_EDUNGEON 10506
#define SC_EXPLORE_DUNGEON_LIST 10505
#define CS_EXPLORE_CHALLENGE_ENCOUNTER 10507
#define SC_EXPLORE_CHALLENGE_ENCOUNTER 10508
#define SC_EXPLORE_DELETE_ENCOUNTER 10509
#define CS_EXPLORE_GIVEUP_ENCOUNTER 10510
#define SC_EXPLORE_GIVEUP_ENCOUNTER 10511
#define CS_EXPLORE_LIST 10512
#define SC_EXPLORE_LIST 10513
#define CS_EXPLORE_COLLECT 10514
#define SC_EXPLORE_COLLECT 10515
#define CS_EXPLORE_FORCE_COLLECT 10516
#define SC_EXPLORE_FORCE_COLLECT 10517
#define CS_EXPLORE_FREE 10530
#define SC_EXPLORE_FREE 10531
#define CS_GER_INFO 10401
#define P_GER 10403
#define SC_GER_INFO 10402
#define P_GER_POS_INFO 10404
#define SC_GER_UPDATE 10405
#define SC_GER_NEW 10406
#define CS_GER_STANDUP 10407
#define SC_GER_STANDUP 10408
#define CS_GER_XIAZHEN 10427
#define SC_GER_XIAZHEN 10428
#define CS_GER_MOVE_POS 10409
#define SC_GER_MOVE_POS 10410
#define CS_GER_POS_LIST 10411
#define SC_GER_POS_LIST 10412
#define CS_ITEM_UP_ALL_EQUIP 10640
#define SC_ITEM_UP_ALL_EQUIP 10641
#define P_GER_POS 10413
#define SC_GER_MORE 10429
#define CS_GER_LIEU_POS_LIST 10450
#define SC_GER_LIEU_POS_LIST 10451
#define CS_GER_LIEU_STANDUP 10452
#define SC_GER_LIEU_STANDUP 10453
#define CS_GER_LIEU_UNTIE 10456
#define SC_GER_LIEU_UNTIE 10457
#define CS_GER_LIEU_INFO_LIST 10459
#define SC_GER_LIEU_INFO_LIST 10460
#define CS_GER_LIEU_MOVE_POS 10461
#define SC_GER_LIEU_MOVE_POS 10462
#define CS_GER_LIEU_LOCK_CLO 10463
#define SC_GER_LIEU_LOCK_CLO 10464
#define CS_GER_LIEU_UNLOCK_CLO 10465
#define SC_GER_LIEU_UNLOCK_CLO 10466
#define CS_GER_LIEU_REFRESH_CLO 10467
#define SC_GER_LIEU_REFRESH_CLO 10468
#define CS_GER_LIEU_TIE_INFO 10469
#define SC_GER_LIEU_TIE_INFO 10470
#define CS_GER_SELL 10414
#define SC_GER_SELL 10415
#define CS_GER_DETAIL 10416
#define SC_GER_DETAIL 10417
#define CS_GER_VIEW_OTHER 10419
#define SC_GER_VIEW_OTHER 10420
#define CS_GER_VIEW_OTHER_DTL 10442
#define SC_GER_VIEW_OTHER_DTL 10443
#define SC_GER_UPDATE_EXP 10421
#define CS_GER_EAT 10422
#define SC_GER_EAT 10423
#define CS_GER_UP_RANK 10424
#define SC_GER_UP_RANK 10425
#define SC_GER_UPDATE_STANDLIST 10426
#define SC_GER_DEL 10430
#define SC_GER_NEW_LIST 10474
#define P_GER_POWER 10441
#define SC_GER_REFRESH_POWER 10440
#define CS_MESSAGE_NOTICE 10302
#define P_NOTICE 10304
#define SC_MESSAGE_NOTICE 10303
#define CS_MESSAGE_CERTAIN_NOTICE 10305
#define SC_MESSAGE_CERTAIN_NOTICE 10306
#define SC_MESSAGE_BC 10301
#define SC_MESSAGE_BC_ID 10307
#define SC_MESSAGE_BC_ID2 10308
#define CS_MESSAGE_TEST 10330
#define SC_MESSAGE_TEST 10331
#define SC_MESSAGE_BEST_CARD 30001
#define SC_MESSAGE_GER_UPLEVEL 30002
#define SC_MESSAGE_ITEM_UPRANK 30003
#define CS_BATTLE_PROGRESS 10201
#define SC_BATTLE_PROGRESS 10202
#define CS_BATTLE_INFO 10203
#define P_DUNGEON 10205
#define SC_BATTLE_INFO 10204
#define CS_BATTLE_CHALLENGE 10206
#define SC_BATTLE_CHALLENGE 10207
#define CS_BATTLE_CHALLENGE_HARD 10213
#define CS_BATTLE_PERFECT_REWARD 10208
#define SC_BATTLE_PERFECT_REWARD 10209
#define CS_BATTLE_DUNGEON_RAIDS 10214
#define SC_BATTLE_DUNGEON_RAIDS 10215

#define CS_BATTLE_RANK      10217
#define SC_BATTLE_RANK      10218

#define SEND_ROLE_CHANGE_NAME 10141
#define RECV_ROLE_CHANGE_NAME 10142
#define CS_ROLE_INFO 10101
#define SC_ROLE_INFO 10102
#define SC_ROLE_UPDATE_LEVEL 10107
#define SC_ROLE_UPDATE_LIST 10103
#define CS_ROLE_BUY_ENERGY 10105
#define SC_ROLE_BUY_ENERGY 10106
#define SC_ROLE_UPDATE_EXP 10108
#define SC_ROLE_UPDATE_COIN 10109
#define SC_ROLE_UPDATE_REPUTATION 10110
#define SC_ROLE_UPDATE_GOLD 10111
#define SC_ROLE_UPDATE_GOLDBONUS 10112
#define SC_ROLE_UPDATE_VIPLEVEL 10113
#define SC_ROLE_UPDATE_ENERGY 10114
#define SC_ROLE_UPDATE_DISCOVERYTIMES 10115
#define SC_ROLE_UPDATE_PVPTIMES 10116
#define SC_ROLE_UPDATE_PLUNDERTIMES 10117
#define SC_ROLE_UPDATE_RANDOMPVPTIMES 10118
#define SC_ROLE_UPDATE_SINGLEPVPTIMES 10119
#define SC_ROLE_UPDATE_GOLDUSED 10120
#define SC_ROLE_UPDATE_TITLE 10121
#define SC_ROLE_UPDATE_ENCOUNTERFREENUM 10122
#define SC_ROLE_UPDATE_WEIBOCOUNT 10123
#define SC_ROLE_UPDATE_WEIBOCOUNT 10123
#define CS_ROLE_PUSH_SETTING 10150
#define SC_ROLE_PUSH_SETTING 10151
#define CS_ROLE_GET_GUIDE_STATE 10153
#define SC_ROLE_GET_GUIDE_STATE 10154
#define CS_ROLE_SET_GUIDE_STATE 10155
#define SC_ROLE_SET_GUIDE_STATE 10156
#define CS_ROLE_LOG_GUIDE_STATE 10136
#define CS_ROLE_CHANGE_HEAD 10157
#define SC_ROLE_CHANGE_HEAD 10158

#define CS_ROLE_TOKEN 10180
#define SC_ROLE_TOKEN 10181
#define CS_ROLE_SELECT_GER 10182
#define SC_ROLE_SELECT_GER 10183
#define CS_ROLE_DEMO_FIGHT 10184
#define SC_ROLE_DEMO_FIGHT 10185
#define SC_ROLE_BASE_CONFIG 10186
#define CS_ROLE_PAY_IOS 10190
#define SC_ROLE_PAY_IOS 10191
#define CS_ROLE_PAY_91 10192
#define SC_ROLE_PAY_91 10193
#define SC_ROLE_PAY_DL 10195
#define SC_ROLE_PAY_ZZ 10196
#define SC_ROLE_PAY_360 10197
#define SC_ROLE_PAY_WDJ 10198
#define SC_ROLE_PAY_DK 10199
#define SC_ROLE_PAY_MI 10189
#define SC_ROLE_PAY_AZ 10188
#define CS_ROLE_SETTING 10124
#define SC_ROLE_SETTING 10125
#define CS_ACCOUNT_LOGIN 10001
#define SC_ACCOUNT_LOGIN 10002
#define CS_ACCOUNT_CREATE 10003
#define SC_ACCOUNT_CREATE 10004
#define CS_ACCOUNT_ENTER_GAME 10005
#define SC_ACCOUNT_ENTER_GAME 10006
#define SC_ACCOUNT_KICK 10007
#define CS_ACCOUNT_HEART 10013
#define SC_ACCOUNT_HEART 10014
#define CS_ROLENAME_CHECK 10016
#define SC_ROLENAME_CHECK 10017
#define SC_ROLE_PAY_UC 10194
#define CS_VERSION 12301
#define SC_VERSION 12302
#define CS_EXPLORE_AUTO_EXPLORE_CHECK 10518
#define SC_EXPLORE_AUTO_EXPLORE_CHECK 10519
#define CS_NANM_OPEN 12401
#define SC_NANM_OPEN 12402
#define SC_NANM_INIT_STATE 12403
#define CS_NANM_CLOSE 12404
#define CS_NANM_BUFF 12405
#define SC_NANM_BUFF 12406
#define CS_NANM_LAST_INFO 12411
#define SC_NANM_LAST_INFO_IGNORE 12412
#define SC_NANM_LAST_INFO_WIN 12413
#define SC_NANM_LAST_INFO_FAIL 12414
#define CS_NANM_CUR_INFO 12416
#define SC_NANM_CUR_INFO_IGNORE 12417
#define SC_NANM_CUR_INFO 12418
#define SC_NANM_HP_SYNC 12420
#define SC_NANM_HARM_BROADCAST 12421
#define SC_NANM_BUFF_SYNC 12424
#define SC_NANM_STOP 12425
#define CS_NANM_RANK_SYNC 12426
#define SC_NANM_RANK_SYNC 12427
#define CS_NANM_FIGHT 12431
#define SC_NANM_FIGHT 12432
#define CS_NANM_REBORN 12441
#define SC_NANM_REBORN 12442
#define CS_NANM_OFFLINE_PLAY 12443
#define SC_NANM_OFFLINE_PLAY 12444
#define CS_NANM_OPEN_TIME 12450
#define SC_NANM_OPEN_TIME 12451
#define CS_ACTIVITY_ENERGY 11943
#define SC_ACTIVITY_ENERGY 11944
#define CS_ROLE_GET_ENERGY 10126
#define SC_ROLE_GET_ENERGY 10127
#define CS_EXPLORE_ENCOUNTER_PASS_REWARD 10520
#define SC_EXPLORE_ENCOUNTER_PASS_REWARD 10521
#define SC_PUSH_HIGHLIGHT_INFO 12501
#define CS_TALK_WORLD 12601
#define SC_TALK_WORLD 12602
#define SC_TALK_WORLD_MESSAGE 12603
#define CS_GAG_ONE 12604
#define CS_UNGAG_ONE 12605
#define CS_GET_GAG_LIST 12606
#define SC_GET_GAG_LIST 12607
#define CS_TALK_RECENT_LIST 12608
#define SC_TALK_RECENT_LIST 12609
#define SC_TALK_PERSON 12611
#define CS_TALK_PERSON 12610
#define CS_TALK_PERSON_OFFLINE 12612
#define SC_TALK_PERSON_OFFLINE 12613

// 神將录，编號：12700 Add By: WenYong
#define CS_CHALLENGEGOD_INFO        12701
#define SC_CHALLENGEGOD_INFO        12702
#define CS_CHALLENGEGOD_SELECT_GER  12703
#define SC_CHALLENGEGOD_SELECT_GER  12704
#define CS_CHALLENGEGOD_CHALLENGE_DUNGEON_ONE 12705
#define SC_CHALLENGEGOD_CHALLENGE_DUNGEON_ONE 12706
#define CS_CHALLENGEGOD_CHALLENGE_DUNGEON_TEN 12707
#define P_CHALLENGEGOD_RESULT           12708
#define SC_CHALLENGEGOD_CHALLENGE_TEN   12709

//帝王
#define CS_EMPEROR_GET_OPEN_TIME 12801
#define SC_EMPEROR_GET_OPEN_TIME 12802
#define CS_EMPEROR_ENTER 12803
#define SC_EMPEROR_ENTER 12804
#define SC_EMPEROR_BROADCAST_FIGHTINFO 12806
#define CS_EMPEROR_REPLAY 12807
#define SC_EMPEROR_REPLAY 12808
#define CS_EMPEROR_QUIT 12809
#define CS_EMPEROR_GET_BET_INFO 12810
#define SC_EMPEROR_GET_BET_INFO 12811
#define CS_EMPEROR_ROLE_BET 12813
#define SC_EMPEROR_ROLE_BET 12814
#define CS_EMPEROR_BET_INFO 12815
#define SC_EMPEROR_BET_INFO 12816
#define CS_EMPEROR_GET_REPLAY 12817
#define SC_EMPEROR_GET_REPLAY 12818
#define SC_EMPEROR_BC_FIGHT_END 12820
#define CS_ACTIVITY_SIGN_EMPEROR_INFO 11945
#define SC_ACTIVITY_SIGN_EMPEROR_INFO 11946
#define CS_ACTIVITY_SIGN_GET_REWARD 11947
#define SC_ACTIVITY_SIGN_GET_REWARD 11948
#define CS_ACTIVITY_SIGN_UP 11949
#define SC_ACTIVITY_SIGN_UP 11950
//買coin
#define CS_ROLE_BUY_COIN_VALUE 10128
#define SC_ROLE_BUY_COIN_VALUE 10129

// 汉帝寶库
#define CS_TREAHOUSE_GET_LIST   13001
#define P_TREAHOUSE_CARD        13002
#define SC_TREAHOUSE_GET_LIST   13003
#define CS_TREAHOUSE_IS_OPEN    13004
#define SC_TREAHOUSE_IS_OPEN    13005
#define CS_TREAHOUSE_EXPLORE_ONE    13006
#define SC_TREAHOUSE_EXPLORE_ONE    13007
#define P_TREAHOUSE_CARD_ONETIME    13008
#define CS_TREAHOUSE_EXPLORE_TEN    13009
#define SC_TREAHOUSE_EXPLORE_TEN    13010
#define CS_TREAHOUSE_REFRESH        13011
#define SC_TREAHOUSE_REFRESH        13012
#define CS_TREAHOUSE_OPEN_BASE_BOX  13013
#define SC_TREAHOUSE_OPEN_BASE_BOX  13014
#define CS_TREAHOUSE_GET_RANK_INFO  13016
#define SC_TREAHOUSE_GET_RANK_INFO  13017
#define CS_TREAHOUSE_GET_RANK_REWARD 13018
#define SC_TREAHOUSE_GET_RANK_REWARD 13019
#define CS_TREAHOUSE_GET_BASEBOXREWARDINFO 13020
#define SC_TREAHOUSE_GET_BASEBOXREWARDINFO 13021
#define P_BASEBOXOPENINFO 13022
#define SC_TREAHOUSE_CHANGE_STATE 13024

// 爆竹
#define CS_FIRECRACKER_OPEN         13101
#define SC_FIRECRACKER_OPEN         13102
#define P_DISCOUNT                  13103
#define SC_FIRECRACKER_INFO_SYNC    13104
#define CS_FIRECRACKER_CLOSE        13105
#define CS_FIRECRACKER_SETOFF       13106
#define SC_FIRECRACKER_SETOFF       13107
#define CS_FIRECRACKER_RANK         13108
#define SC_FIRECRACKER_RANK         13109
#define P_FIRECRACKER_RANK          13110
#define CS_FIRECRACKER_GET_REWARD   13111
#define SC_FIRECRACKER_GET_REWARD   13112

// 新年返利
#define CS_ACTIVITY_REBATE_INFO     11951
#define SC_ACTIVITY_REBATE_INFO     11952
#define P_REBATE_LIST               11953
#define P_REBATE_INFO               11954
#define CS_ACTIVITY_REBATE_GET_REWARD 11955
#define SC_ACTIVITY_REBATE_GET_REWARD 11956
#define P_REBATE_REWARD             11957
#define SC_REBATE_UPDATE            11958

//问鼎
#define SC_CROSS_NEW_FIGHT 12901
#define CS_CROSS_SIGN 12910
#define SC_CROSS_SIGN 12911
#define CS_CROSS_INFO 12920
#define SC_CROSS_INFO 12921
#define CS_CROSS_ENTER 12922
#define CS_CROSS_LEAVE 12923
#define CS_CROSS_VIEW_LIST 12924
#define SC_CROSS_VIEW_LIST 12925
#define CS_CROSS_SUPPORT 12930
#define SC_CROSS_SUPPORT 12931
#define CS_CROSS_PRICE_LIST 12932
#define SC_CROSS_PRICE_LIST 12933
#define CS_CROSS_REPLAY 12905
#define SC_CROSS_REPLAY 12906
#define CS_CROSS_HISTORY 12903
#define SC_CROSS_HISTORY 12904
#define SC_CROSS_FIGHT_LIST 12907
//副將刷新
#define CS_GER_LIEU_REFRESH_FREETIMES 10472
#define SC_GER_LIEU_REFRESH_FREETIMES 10473

//冲級活動
#define CS_ACTIVITY_LEVELRANK_OPEN 11959
#define SC_ACTIVITY_LEVELRANK_OPEN 11960
#define CS_ACTIVITY_LEVELRANK_REFRESH 11962
#define SC_ACTIVITY_LEVELRANK_REFRESH 11963
#define CS_ROLE_WEIXIN_SHARE 10130

//疼恤購買
#define SC_BUY_YYB 10222

//卡牌合成
#define CS_COMBINE_DO   13201
#define SC_COMBINE_FAIL 13202
#define SC_COMBINE_GER  13203
#define SC_COMBINE_EQUIP 13204
#define P_NEWGER    13205
#define P_NEWEQUIP  13206

//首儲双倍push通知
#define SC_ROLE_UPDATE_PAY_EXT 10131
#define CS_ACTIVITY_DAY_PAY_MUL 11964
#define SC_ACTIVITY_DAY_PAY_MUL 11965
#define CS_ROLE_PAY_GET_ORDER 10178
#define SC_ROLE_PAY_GET_ORDER 10179
//地理位置改变
#define CS_ROLE_CHANGE_LOCATION 10159

#endif
