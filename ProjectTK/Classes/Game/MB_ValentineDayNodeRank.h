//
//  MB_ValentineDayNodeRank.h
//  ProjectMB
//
//  Created by wenyong on 15-1-22.
//
//

#ifndef __ProjectMB__MB_ValentineDayNodeRank__
#define __ProjectMB__MB_ValentineDayNodeRank__

#include "MB_NodeFestivalRank.h"

class MB_ValentineDayNodeRank : public MB_NodeFestivalRank
{
public:
    virtual bool init();
};

#endif /* defined(__ProjectMB__MB_ValentineDayNodeRank__) */
